﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Microsoft.AspNetCore.Mvc.RazorPages;


namespace webmusic.Pages
{
    public class IndexModel : PageModel
    {
        public static string root = "music";
        public string path = "";
        public string displaypath = "";
        public string path_oneup = "";
        public string fullpath = "";
        public List<string> dirs = new List<string>();
        public List<string> files = new List<string>();
        public void OnGet()
        {
            if (Request.QueryString.HasValue)
                path = HttpUtility.UrlDecode(Request.QueryString.Value.Remove(0,1)
                    .Replace("+", "%2B"));
            if (path.EndsWith(".m3u"))
                path = path.Substring(0, path.Length - 4);
            if (path.Contains(".."))
            {
                Response.Redirect("/Error");
                return;
            }
            path_oneup = Regex.Match(path, @".*(?=\/)").Value;
            fullpath = root + path;
            displaypath = string.IsNullOrWhiteSpace(path) ? "/" : path;
            dirs = Directory.GetDirectories(fullpath).Select(Path.GetFileName).ToList();
            dirs.Sort();
            files = Directory.GetFiles(fullpath).Select(Path.GetFileName).ToList();
            files.RemoveAll(p => p.EndsWith(".m3u"));
            files.Sort(new AlphanumComparatorFast());
        }

        public string Encode(string str)
        {
            return str.Replace("\"", "%22").Replace("'", "%27")
                .Replace("?", "%3F").Replace("&", "%26")
                .Replace(" ", "%20");
        }

        public class AlphanumComparatorFast : IComparer<string>
        {
            public int Compare(string x, string y)
            {
                string s1 = x;
                if (s1 == null)
                {
                    return 0;
                }

                if (!(y is string s2))
                {
                    return 0;
                }

                int len1 = s1.Length;
                int len2 = s2.Length;
                int marker1 = 0;
                int marker2 = 0;

                // Walk through two the strings with two markers.
                while (marker1 < len1 && marker2 < len2)
                {
                    char ch1 = s1[marker1];
                    char ch2 = s2[marker2];

                    // Some buffers we can build up characters in for each chunk.
                    char[] space1 = new char[len1];
                    int loc1 = 0;
                    char[] space2 = new char[len2];
                    int loc2 = 0;

                    // Walk through all following characters that are digits or
                    // characters in BOTH strings starting at the appropriate marker.
                    // Collect char arrays.
                    do
                    {
                        space1[loc1++] = ch1;
                        marker1++;

                        if (marker1 < len1)
                        {
                            ch1 = s1[marker1];
                        }
                        else
                        {
                            break;
                        }
                    } while (char.IsDigit(ch1) == char.IsDigit(space1[0]));

                    do
                    {
                        space2[loc2++] = ch2;
                        marker2++;

                        if (marker2 < len2)
                        {
                            ch2 = s2[marker2];
                        }
                        else
                        {
                            break;
                        }
                    } while (char.IsDigit(ch2) == char.IsDigit(space2[0]));

                    // If we have collected numbers, compare them numerically.
                    // Otherwise, if we have strings, compare them alphabetically.
                    string str1 = new string(space1);
                    string str2 = new string(space2);

                    int result;

                    if (char.IsDigit(space1[0]) && char.IsDigit(space2[0]))
                    {
                        int thisNumericChunk = int.Parse(str1);
                        int thatNumericChunk = int.Parse(str2);
                        result = thisNumericChunk.CompareTo(thatNumericChunk);
                    }
                    else
                    {
                        result = str1.CompareTo(str2);
                    }

                    if (result != 0)
                    {
                        return result;
                    }
                }
                return len1 - len2;
            }
        }
    }
}